<?php

namespace App\Http\Controllers\Auth;

use App\Http\Controllers\Controller;
use Illuminate\Foundation\Auth\AuthenticatesUsers;
use Auth;
use App\Models\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Input;
use Log;
use Validator;
class LoginController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Login Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles authenticating users for the application and
    | redirecting them to your home screen. The controller uses a trait
    | to conveniently provide its functionality to your applications.
    |
    */

    use AuthenticatesUsers;

    /**
     * Where to redirect users after login.
     *
     * @var string
     */
    protected $redirectTo = '/home';

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('guest')->except('logout');
    }
    public function showLoginForm(Request $request)
    {
        $this->loginViaRemoteUser($request);
        if (Auth::check()) {
            return redirect()->intended('dashboard');
        }

        return view('auth.login');
    }

    private function loginViaRemoteUser(Request $request)
    {
        $remote_user = $request->server('REMOTE_USER');
//        if (Setting::getSettings()->login_remote_user_enabled == "1" && isset($remote_user) && !empty($remote_user)) {
//            Log::debug("Authenticatiing via REMOTE_USER.");

            $pos = strpos($remote_user, '\\');
            if ($pos > 0) {
                $remote_user = substr($remote_user, $pos + 1);
            };

            try {
                $user = User::where('username', '=', $remote_user)->whereNull('deleted_at')->first();
                Log::debug("Remote user auth lookup complete");
                if(!is_null($user)) Auth::login($user, true);
            } catch(Exception $e) {
                Log::debug("There was an error authenticating the Remote user: " . $e->getMessage());
            }
    }

    public function login(Request $request)
    {
   $validator = $this->validator(Input::all());
   if ($validator->fails()){
       return redirect()->back()->withInput()->withErrors($validator);
   }
   $users = null;
   if (!$users){
      if (!Auth::attempt(['username' => $request->input('username'), 'password'=> $request->input('password')], $request->input('remember'))){
          return redirect()->back()->withInput()->with('errors',trans('auth.account_not_found'));
      }
      else
          {
          $this->clearLoginAttempts($request);
      }
   }
   if ($users == che)
    }

    protected function validator(array $data){
        return Validator::make($data,[
            'username'=>'required',
            'password'=>'required'
        ]);
    }
}
