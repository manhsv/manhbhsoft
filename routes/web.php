<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Route::group(['middleware'=>'web'],function (){
    Route::get('login',[
        'as'=>'login',
        'middleware'=>['web'],
        'users'=>'Auth/LoginController@showLoginForm'
    ]);
    Route::post('login',[
        'as'=>'login',
    'middleware'=>['web'],
    'users'=>'Auth/LoginController@login'
        ]);
});
Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');
